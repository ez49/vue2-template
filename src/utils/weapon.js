/**
 * 工具函数集合
 */

/**
 * 获取 x 天前的日期
 * @param {number} dayCount 天数
 * @returns {string} 格式为'2021-01-01'
 */
export function getTragetTime(dayCount) {
  let time = new Date().getTime() - dayCount * 24 * 60 * 60 * 1000;
  let tragetTime = new Date(time);
  let month = tragetTime.getMonth() > 9 ? `${tragetTime.getMonth() + 1}` : `0${tragetTime.getMonth() + 1}`;
  let day = tragetTime.getDate() > 9 ? `${tragetTime.getDate()}` : `0${tragetTime.getDate()}`;
  return `${tragetTime.getFullYear()}-${month}-${day}`
}
