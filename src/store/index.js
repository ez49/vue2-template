import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  count: 0,
}
const getters = {}
const mutations = {
  increment(state) {
    state.count++
  },
}
const actions = {}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  //   modules: { community, s3, openIM, shift, hims, dfPMS, himsChat, BCC, reservaCar },
})
