# vue2 项目模板

This template should help get you started developing with Vue Swan-Song version（2.7.16） in Vite.

## Environment

Node version > 20.x

移动端表格组件 [vue-mobile-table](https://www.npmjs.com/package/vue-mobile-table)

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Vue-Official](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
